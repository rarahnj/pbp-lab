from django.http.response import HttpResponseRedirect
from django.shortcuts import redirect, render
from .models import Note # import model note yg dibuat di models.py
from .forms import NoteForm
# Create your views here.

def index(request):
    Notes = Note.objects.all()  # syntax SQL buat narik object dari database
    response = {'Notes': Notes} # ini string yg dikirim
    return render(request, 'lab4_index.html', response)


def add_note(request):
    form = NoteForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()     
        return HttpResponseRedirect ('/lab-4')
    else:
        form = NoteForm()
    return render(request, 'lab4_form.html', {'form': form})


def note_list(request):
    Notes = Note.objects.all()  # syntax SQL buat narik object dari database
    response = {'Notes': Notes} # ini string yg dikirim
    return render(request, 'lab4_note_list.html', response)