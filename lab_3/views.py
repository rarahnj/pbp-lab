from django.http.response import HttpResponseRedirect, HttpResponseRedirectBase
from django.shortcuts import render
from .models import Friend
from .forms import FriendForm

# imported login required
from django.contrib.auth.decorators import login_required

# Create your views here.

@login_required(login_url='/admin/login/')
def index (request):
    
    friends = Friend.objects.all()  # TODO Implement this, syntax SQL buat narik object dari database
    response = {'friends': friends} # ini string yg dikirim

    return render(request, 'lab3_index.html', response)

# https://docs.djangoproject.com/en/3.2/topics/forms/
@login_required(login_url='/admin/login/')
def add_friend(request):
        # create a form instance and populate it with data from the request:
        form = FriendForm(request.POST or None)
        # check whether it's valid:
        # if this is a POST request we need to process the form data
        if (form.is_valid() and request.method == 'POST'):
            # process the data in form.cleaned_data as required
            # save if valid
            form.save()
            # redirect to a new URL:
            return HttpResponseRedirect('/lab-3')

    # if a GET (or any other method) we'll create a blank form
        else:
            form = FriendForm()

        return render(request, 'lab3_form.html', {'form': form})