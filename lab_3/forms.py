from django import forms
from django.forms import widgets
from django.forms.widgets import DateInput 
from lab_3.models import Friend


class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = ['name','npm','tanggalLahir']

        name = forms.CharField(max_length='30')
        npm = forms.CharField(max_length='10')
        tanggalLahir = forms.DateField()

    error_messages = {
		'required' : 'Please Type'
	}

    widgets = {
            'tanggalLahir': DateInput(),
        }


