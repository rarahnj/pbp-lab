from django.db import models

# Create your models here.

class Note(models.Model):
    to = models.CharField(max_length=50)
    from_attribute = models.CharField(max_length=50)
    title = models.CharField(max_length=50)
    message = models.TextField()
    
