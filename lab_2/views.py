from lab_1.views import index
from django.shortcuts import render
from .models import Note # import model note yg dibuat di models.py
from django.http.response import HttpResponse
from django.core import serializers # ini buat convert html ke xml
# Create your views here.

def index(request):
    Notes = Note.objects.all()  # syntax SQL buat narik object dari database
    response = {'Notes': Notes} # ini string yg dikirim
    return render(request, 'lab2.html', response)

def xml(request):
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(request):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")