# logic untuk pengolahan data dari models

from django.shortcuts import render
from datetime import datetime, date
from .models import Friend

mhs_name = 'Raudhotul Jannah'  # done, first things to do, fill this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2001, 8, 1)  # done
npm = 2006536220  # done


def index(request):
    response = {'name': mhs_name,
                'age': calculate_age(birth_date.year),
                'npm': npm}
    return render(request, 'index_lab1.html', response)


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0


def friend_list(request):
    friends = Friend.objects.all()  # TODO Implement this, syntax SQL buat narik object dari database
    response = {'friends': friends} # ini string yg dikirim
    return render(request, 'friend_list_lab1.html', response)
