from django.shortcuts import render

# Create your views here.
from lab_4.models import Note # import model note yg dibuat di models.py
# from .forms import NoteForm
# Create your views here.

def index(request):
    Notes = Note.objects.all()  # syntax SQL buat narik object dari database
    response = {'Notes': Notes} # ini string yg dikirim
    return render(request, 'lab5_index.html', response)