1. Apakah perbedaan antara JSON dan XML?
    JSON merupakan bahasa yang lebih mudah 'dibaca' dibandingkan XML, sedangkan dokumen XML sendiri biasanya sulit untuk dibaca atau dimengerti, akan tetapi dibandingkan JSON, XML lebih secure untuk digunakan. Kemudian XML mendukung beberapa jenis encoding, sedangan JSON hanya mendukung UTF-8 encoding. Lalu JSON supports Array sedangan XML tidak.
    Json digunakan untuk mempresentasikan objek, sedangakn XML digunakan untuk mempresentasikan data items. Selain itu JSON tidak mendukung adanya komentar, sedangkan XML mendukung untuk adanya komentar.


2. Apakah perbedaan antara HTML dan XML?
    HTML merupakan Hyper Text Markup Language, sedangakan XML merupakan Extensible Markup Language, keduanya digunakan untuk membuat halaman dan aplikasi web. HTML merupakan bahasa yang statis karena digunakan untuk menampilkan data dan bukan menyalurkan data, sedangkan XML merupakan bahasa yang dinamis karena digunakan untuk menyimpan dan menyalurkan data ke dan dari database dan bukan menampilkan data.
    HTML sendiri bisa mengabaikan error kecil dan tidak case sensitive, sedangkan XML case sensitive dan tidak bisa berjalan jika ada error. Tags yang berada pada HTML digunakan untuk menampilkan data, sedangkan pada XML, tags digunakan untuk menjelaskan data. 



Sumber :
- https://www.geeksforgeeks.org/difference-between-json-and-xml/
- https://www.geeksforgeeks.org/html-vs-xml/
