import 'package:flutter/material.dart';

Map<int, Color> color =
{
  50: Color(0xFFECEFF1),
  100: Color(0xFFCFD8DC),
  200: Color(0xFFB0BEC5),
  300: Color(0xFF90A4AE),
  400: Color(0xFF78909C),
  500: Color(0xFF9E9E9E),
  600: Color(0xFF546E7A),
  700: Color(0xFF455A64),
  800: Color(0xFF37474F),
  900: Color(0xFF263238),
};

void main() {
  runApp(const MyApp());
}

// referensi https://belajarflutter.com/penggunaan-bottom-navigation-bar-pada-flutter/

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    MaterialColor colorCustom = MaterialColor(0xFFECEFF1, color);
    return MaterialApp(
      title: 'KonvaSearch',
      theme: ThemeData( //ini Theme widget
        // This is the theme of your application.

        primarySwatch: colorCustom, //default color

        fontFamily: "Roboto", // default font

        textTheme: const TextTheme(
          headline1: TextStyle(fontSize: 40, color: Color.fromRGBO(255, 0, 0, 1), ),
          headline2: TextStyle(fontSize: 40, color: Color.fromRGBO(204, 23, 40, 1),),
        ),

      ),
      home: const MyHomePage(title: 'KonvaSearch'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _selectedNavbar = 0;

  void _changeSelectedNavBar(int index) {
    setState(() {
      _selectedNavbar = index;
    });
  }

  //source : https://www.codingtive.com/2019/05/tutorial-flutter-cara-membuat-bottom-navigation-bar.html
  final _listPage = <Widget>[
      Text("Donasi"),
      HomePage(),
      Tanya(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: RichText(
          text: TextSpan(
            children: <TextSpan>[
              TextSpan(
                text: "Konva", 
                style: Theme.of(context).textTheme.headline1),
              TextSpan(
                text: "Search", 
                style: Theme.of(context).textTheme.headline2)
            ]
          ),
        ),
      ),
      backgroundColor: Color.fromRGBO(220, 234, 249, 1),
      body: Center(
        child: _listPage[_selectedNavbar]
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.health_and_safety),
            //title: Text('Donasi'),
            label: "Donasi",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: "Home",
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.help),
            label: "FAQ",
          ),
        ],

        currentIndex: _selectedNavbar,
        selectedItemColor: Colors.red,
        unselectedItemColor: Colors.grey,
        showUnselectedLabels: true,
        onTap: _changeSelectedNavBar,
      ),
    );
  }
}

//referensi https://belajarflutter.com/memahami-stack-widget-pada-flutter/
class HomePage extends StatelessWidget{
  var date = DateTime.now();
  @override
   Widget build(BuildContext context) {
   return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
                child: Image.asset('assets/images/blood-crimson.png'),
          ),
          Positioned(
            left: 40.0,
            top: 15.0,
            child: Text("Donasi",
                style: TextStyle(color: Color(0xFF263238), fontSize: 35.0)),
          ),
          Positioned(
            left: 40.0,
            top: 50.0,
            child: Text("Plasma",
                style: TextStyle(color: Color(0xFF263238), fontSize: 35.0)),
          ),
          Positioned(
            left: 40.0,
            top: 85.0,
            child: Text("Konvalesen",
                style: TextStyle(color: Color(0xFF263238), fontSize: 35.0)),
          ),
        ],
      ),
    );
  }
}

// referensi https://belajarflutter.com/tutorial-cara-membuat-form-di-flutter-lengkap/
class Tanya extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(20.0),
          child: Column(
            children: [
              // TextField(),
              TextFormField(
                decoration: new InputDecoration(
                  labelText: "Nama",
                  icon: Icon(Icons.people),
                  border: OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(5.0)),
                ),
              ),
              RaisedButton(
                child: Text(
                  "Submit",
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.blue,
                onPressed: () {
                  //if (_formKey.currentState!.save()) {}
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}